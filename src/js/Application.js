import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }
  setEmojis(emojis) {
    this.emojis = emojis;
  }

  addBananas() {
    this.monkeyWithBananas = this.emojis.map((element) => {
      return element += this.banana;
    });
    const display = document.createElement("p");
    display.textContent = this.monkeyWithBananas;
    const wrapper = document.querySelector(".emojis");
    wrapper.appendChild(display);
  }
}
